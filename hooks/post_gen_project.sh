#!/bin/bash

# push to remote repo
git init
git add -A
git commit -m 'Initial commit'
git push --set-upstream git@gitlab.com:{{cookiecutter.gitlab_account}}/{{cookiecutter.repo_name}}.git master
